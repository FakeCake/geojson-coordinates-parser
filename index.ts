class Parser {
    coordinates: Array<Number>;

    constructor(public x: Number, public y: Number) {
        this.coordinates = [x, y];
    }

    public toObject() {
        return {
            'x': this.x,
            'y': this.y,
            'coordinates': this.coordinates,
        };
    }
}

export default Parser;
