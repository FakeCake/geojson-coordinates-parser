# Geojson-Coordinates-Parser

[![pipeline status](https://gitlab.com/FakeCake/geojson-coordinates-parser/badges/main/pipeline.svg)](https://gitlab.com/FakeCake/geojson-coordinates-parser/-/commits/main)
[![coverage report](https://gitlab.com/FakeCake/geojson-coordinates-parser/badges/main/coverage.svg)](https://gitlab.com/FakeCake/geojson-coordinates-parser/-/commits/main) 

Convert X Y points to Lat Lng, or Lat Lng points to  X Y.
