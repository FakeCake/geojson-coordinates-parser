import Parser from './index';

describe('toObject', ()=>{
    it('should return class properties in object', ()=>{
        expect(new Parser(1, 2).toObject()).toStrictEqual({'x': 1, 'y': 2, 'coordinates': [1, 2]});
    });
});
